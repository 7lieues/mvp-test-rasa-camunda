package com.lieues.workflow.dao;

import com.lieues.workflow.entities.Conge;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

@RestResource
public interface CongeRepo extends JpaRepository<Conge,Long> {
}
