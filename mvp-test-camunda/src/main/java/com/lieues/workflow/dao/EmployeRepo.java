package com.lieues.workflow.dao;

import com.lieues.workflow.entities.Employe;
import com.lieues.workflow.entities.Manager;
import org.springframework.data.repository.query.Param;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

@RestResource
public interface EmployeRepo extends JpaRepository<Employe,Long> {
    @Query("SELECT e FROM Employe e WHERE e.name=:name ")
    public Employe getEmployeByName(@Param("name") String name);

    @Query("SELECT m FROM Manager m join m.Personnel e WHERE e.id=:idmanger")
    public Manager getManagerByEmplye(@Param("idmanger") Long idmanger);

}
