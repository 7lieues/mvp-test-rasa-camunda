package com.lieues.workflow.delegates;

import com.lieues.workflow.dao.EmployeRepo;
import com.lieues.workflow.entities.Employe;
import com.lieues.workflow.entities.Manager;
import com.lieues.workflow.services.IRasaService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RefusDemande implements JavaDelegate {
    @Autowired
    private EmployeRepo employeRepo;
    @Autowired
    private IRasaService iRasaService;

    private final Logger LOGGER = LoggerFactory.getLogger(RefusDemande.class);



    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        String date_debut = (String) delegateExecution.getVariable("date_debut");
        String nbr_jours = (String) delegateExecution.getVariable("nbr_jours");
        String taskid = (String) delegateExecution.getId();
        System.out.println("================== taskid ==========="+taskid);
        String trackerID = (String) delegateExecution.getVariable("trackerID");
        System.out.println("================== trackerID ==========="+taskid);
        String[] decoup = trackerID.split("@", 2);
        String name = decoup[0];
        Employe emp = employeRepo.getEmployeByName(name);
        System.out.println("===============Employer name==========="+emp.getName());
        Manager manager =employeRepo.getManagerByEmplye(emp.getId());
        System.out.println("===============Manager name==========="+manager.getName());
        LOGGER.info("ici c'est ================ RefusDemande ==========");


        LOGGER.info("==========================");
        LOGGER.info("action_listen");
        LOGGER.info("==========================");

        iRasaService.fill_slots(manager,"mng_date_debut",date_debut);
        iRasaService.fill_slots(manager, "mng_nbr_jours",nbr_jours);
        // rasaActionListen(trackerID);
        LOGGER.info("==========================");
        LOGGER.info("action_trigger");
        LOGGER.info("==========================");

        iRasaService.utter( manager);
    }










}
