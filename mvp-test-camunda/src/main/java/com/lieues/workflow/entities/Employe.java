package com.lieues.workflow.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import javax.swing.*;
import java.io.Serializable;
import java.util.List;

@Entity @Data @NoArgsConstructor @AllArgsConstructor @ToString
public class Employe implements Serializable {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String email;
    @ManyToOne
    private Manager manger;

    @OneToMany(mappedBy="employe")
    private List<Conge> conge;
}
