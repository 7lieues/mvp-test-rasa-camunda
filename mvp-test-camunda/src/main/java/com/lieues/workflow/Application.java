package com.lieues.workflow;

import com.lieues.workflow.dao.CongeRepo;
import com.lieues.workflow.dao.EmployeRepo;
import com.lieues.workflow.dao.MangerRepo;
import com.lieues.workflow.entities.Conge;
import com.lieues.workflow.entities.Employe;
import com.lieues.workflow.entities.Manager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@SpringBootApplication
public class Application {

  public static void main(String... args) {
    SpringApplication.run(Application.class, args);
  }
  @Autowired
  MangerRepo mangerRepo;
  @Bean
  public RestTemplate restTemplate() {
    return new RestTemplate();
  }

  @Bean
  CommandLineRunner start(CongeRepo congeRepo , EmployeRepo employeRepo , MangerRepo mangerRepo){
    return args -> {
     

      Manager olivier =new Manager(null,"Olivier","olivier@7lieues.io",null);
      mangerRepo.save(olivier);

      Employe ilyes =new Employe(null,"ilyes","ilyes@7lieues.com",olivier,null);
      Employe yasser =new Employe(null,"yasser","yasser@7lieues.com",olivier,null);

      employeRepo.save(ilyes);
      employeRepo.save(yasser);

    };
  }

}