package com.lieues.workflow.services;

import com.lieues.workflow.entities.Manager;

public interface IRasaService {

    public void fill_slots( Manager man, String nameSlot, String valueSlote );
    public void utter(Manager man) throws Exception;
    public void setslot( Manager man, String task_id) ;
}
