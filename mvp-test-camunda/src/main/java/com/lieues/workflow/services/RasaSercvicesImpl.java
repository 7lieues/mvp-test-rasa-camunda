package com.lieues.workflow.services;

import com.google.gson.Gson;
import com.lieues.workflow.dao.EmployeRepo;
import com.lieues.workflow.entities.Manager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.HashMap;
import java.util.Map;
@Service
public class RasaSercvicesImpl implements IRasaService{

    @Autowired
    private Gson gson;
    @Autowired
    private EmployeRepo employeRepo;
    @Autowired
    private RestTemplate restTemplate;


    @Override
    public void fill_slots( Manager man, String nameSlot, String valueSlot) {
        HttpHeaders requestHeader = new HttpHeaders();
        requestHeader.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        String manager_tracker_id = (man.getEmail()+"::2::0::webchat");
        System.out.println("***********************"+manager_tracker_id+"***********************");

        Map<String, Object> inputMap = new HashMap<>();

        inputMap.put("name", nameSlot);
        inputMap.put("event", "slot");
        inputMap.put("value", valueSlot);
        inputMap.put("timestamp", 0);

        String jsonProduct = gson.toJson(inputMap);
        HttpEntity<String> requestEntitySetSlot = new HttpEntity<>(jsonProduct, requestHeader);

        StringBuilder rasaURISlot = new StringBuilder();

        rasaURISlot.append("http://mvp-test-rasa:5005/conversations/").append(manager_tracker_id).append("/tracker/events");
        UriComponentsBuilder rasaURISlotBuilder = UriComponentsBuilder.fromUriString(rasaURISlot.toString());
        restTemplate.postForEntity(rasaURISlotBuilder.toUriString(), requestEntitySetSlot, Map.class);

        System.out.println("*********************** SLOT "+nameSlot+" SET ***********************");



    }

    @Override
    public void utter(Manager man) throws Exception {
        HttpHeaders requestHeader = new HttpHeaders();
        requestHeader.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        String manager_tracker_id = (man.getEmail()+"::2::0::webchat");
        System.out.println("***********************"+manager_tracker_id);

        Map<String, String> Map3 = new HashMap<>();
        Map3.put("name", "confirmation_manager");
        String json3 = gson.toJson(Map3);
        HttpEntity<String> requestEntitySetSlot3 = new HttpEntity<>(json3, requestHeader);
        StringBuilder rasaURISlot2 = new StringBuilder();
        rasaURISlot2.append("http://mvp-test-rasa:5005/conversations/").append(manager_tracker_id).append("/trigger_intent?output_channel=callback");
        UriComponentsBuilder rasaURISlotBuilder3 = UriComponentsBuilder.fromUriString(rasaURISlot2.toString());
        restTemplate.postForEntity(rasaURISlotBuilder3.toUriString(), requestEntitySetSlot3, Map.class);

    }

    public void setslot(Manager man, String task_id)  {
        HttpHeaders requestHeader = new HttpHeaders();
        requestHeader.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        String manager_tracker_id = (man.getEmail() + "::2::0::webchat");
        System.out.println("***********************" + manager_tracker_id + "***********************");

        Map<String, Object> inputMap = new HashMap<>();

        inputMap.put("name", "task_id");
        //inputMap.put("event", "slot");
        inputMap.put("value", task_id);
        //inputMap.put("timestamp", 0);

        String jsonProduct = gson.toJson(inputMap);
        HttpEntity<String> requestEntitySetSlot = new HttpEntity<>(jsonProduct, requestHeader);

        StringBuilder rasaURISlot = new StringBuilder();

        rasaURISlot.append("http://mvp-test-rasa:5005/conversations/").append(manager_tracker_id).append("/tracker/events");
        UriComponentsBuilder rasaURISlotBuilder = UriComponentsBuilder.fromUriString(rasaURISlot.toString());
        restTemplate.postForEntity(rasaURISlotBuilder.toUriString(), requestEntitySetSlot, Map.class);
    }



}
