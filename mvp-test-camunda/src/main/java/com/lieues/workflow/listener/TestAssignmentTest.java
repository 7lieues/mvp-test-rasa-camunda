package com.lieues.workflow.listener;

import com.lieues.workflow.dao.EmployeRepo;
import com.lieues.workflow.delegates.RefusDemande;
import com.lieues.workflow.entities.Employe;
import com.lieues.workflow.entities.Manager;
import com.lieues.workflow.services.IRasaService;
import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateTask;
import org.camunda.bpm.engine.delegate.TaskListener;
import org.camunda.bpm.engine.identity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TestAssignmentTest implements TaskListener {
    @Autowired
    private IRasaService iRasaService;
    @Autowired
    private EmployeRepo employeRepo;
    @Autowired
    IdentityService identityService;

    private final Logger LOGGER = LoggerFactory.getLogger(TestAssignmentTest.class);

    @Override
    public void notify(DelegateTask delegateTask) {



        String assignee =delegateTask.getAssignee();
        LOGGER.info("==========================");
        LOGGER.info("==========================");
        LOGGER.info("===========assignee : ==============="+ assignee);
        LOGGER.info("==========================");
        LOGGER.info("==========================");
       String taskid = delegateTask.getId();
        LOGGER.info("==========================");
        LOGGER.info("==========================");
        LOGGER.info("===========taskid : ==============="+ taskid);
        LOGGER.info("==========================");
        LOGGER.info("==========================");

       String trackerID = (String) delegateTask.getVariable("trackerID");

        String[] decoup = trackerID.split("@", 2);
        String name = decoup[0];
        Employe emp = employeRepo.getEmployeByName(name);
        Manager manager =employeRepo.getManagerByEmplye(emp.getId());

        User user = identityService.createUserQuery().userId(assignee).singleResult();

        LOGGER.info("==========================");
        LOGGER.info("==========================");
        // LOGGER.info("===========taskid : ==============="+ user.getId());
        LOGGER.info("==========================");
        LOGGER.info("==========================");
        iRasaService.fill_slots(manager,"cuisine",taskid);


    }
}
