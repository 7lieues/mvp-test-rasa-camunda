#!/bin/bash -i


image_choice=$1
possible_values=$(docker images | grep "rasa" | grep -v sdk | awk '{print $1 ":" $2}')

if [ -z "$image_choice" ]; then
	echo "Veuillez fournir un argument à ce script"
	echo "Valeurs possibles : "
	echo "$possible_values"

	while : 
  	do
		echo -e "\n"
    		read -p "[RACCOURCI] Souhaitez-vous utiliser l'image <7lieues/mvp-rasa:2.2.0> ? [y/n]
		" choice_p
		case $choice_p in
			[Yy]* )
				image_choice="7lieues/mvp-rasa:2.2.0"
				break
				;;
			[Nn]* )
				echo -e "\n"
				read -p "Veuillez saisir une valeur dans ce cas	" input_val
				image_choice="${input_val}"
				break
				;;
		esac
	done
fi

for valeurs in $possible_values
do
	if [ "$image_choice" = "$valeurs" ]; then
		echo "******** On entraîne le modèle RASA sur les nouvelles conversations/domain/NLU *********"
		echo "docker run -v $(pwd):/app -e TZ=Europe/Paris $1 train --force -c config.yml --out models"
        	docker run \
		-e TZ=Europe/Paris \
		--user $(id -u) \
        	-v $(pwd):/app \
        	${image_choice} \
        	train \
		--force \
		-c config.yml \
        	--out models
		exit 0
	fi
done
echo "Aucune valeur trouvée parmi ${possible_values} dans votre saisie clavier ($input_val)"
