from typing import Any, Text, Dict, List
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
import logging
import json,requests
logger = logging.getLogger(__name__)

class ActionUtterSubmit(Action):
    def name(self):
        return "action_utter_submit"

    def run(
        self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> List[Dict[List, Any]]:

        logger.info( " =========  action_utter_submit  =================  ")


        
        trackerId = tracker.sender_id
        cuisine = tracker.get_slot("cuisine")
        number = tracker.get_slot("number")

        data = json.dumps({"variables": {
            "trackerID":{"value":trackerId,"type":"string"},
            "cuisine":{"value":cuisine,"type":"string"},
            "number":{"value":number,"type":"string"}}})

        headers={"content-type": "application/json"}

        # first_requete = requests.get(
        #     url="http://mvp-test-camunda:8081/managers",
        #     headers=headers
        #     # data=data
        # )
        # logger.info(first_requete.text)

        first_requete = requests.post(
            url="http://mvp-test-camunda:8081/engine-rest/process-definition/key/my-first-project-process/start",
            headers=headers,
            data=data
        )
        logger.info(first_requete.text)

        dispatcher.utter_message(text="Sumbit working")
        return

class ActionGiveYouAction(Action):
    def name(self):
        return "action_give_you_action"

    def run(
        self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> List[Dict[List, Any]]:

        dispatcher.utter_message(text="Salut bo gosse yo, tu as trigger une super action")
        return


class action_via_intent_poc(Action):
    def name(self):
        return "action_via_intent_poc"

    def run(
        self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> List[Dict[List, Any]]:

        logger.info( " \n\n\n ")
        logger.info( " =========  action_utter_submit  =================  ")
        logger.info( " \n\n\n ")

        dispatcher.utter_message(text="action_via_intent_poc working ")
        return
###################  submit forms ask congé process actions   ###################

class action_submit_congé(Action):
    def name(self):
        return "action_submit_congé"

    def run(
        self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> List[Dict[List, Any]]:

        trackerId = tracker.sender_id
        nbr_jours = tracker.get_slot("nbr_jours")
        date_debut = tracker.get_slot("date_debut")

        data = json.dumps({"variables": {
            "trackerID":{"value":trackerId,"type":"string"},
            "nbr_jours":{"value":nbr_jours,"type":"string"},
            "date_debut":{"value":date_debut,"type":"string"}}})
        headers={"content-type": "application/json"}

        try:
            first_requete = requests.post(
                    url="http://mvp-test-camunda:8081/engine-rest/process-definition/key/my-first-project-process/start",
                    headers=headers,
                    data=data,
                    timeout=0.0000000001
                )
        except requests.exceptions.ReadTimeout: 
            pass
        
        logger.info( " \n\n\n ")
        # logger.info(first_requete.text)
        logger.info( " =========  action_submit_congé  =================  ")
        logger.info( " \n\n\n ")

        dispatcher.utter_message(text="Votre demande est transmit à votre manager, veuiller attendre sa reponse ^^ ! ")
        return

class action_submit_confirmation(Action):
    def name(self):
        return "action_submit_confirmation"

    def run(
        self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> List[Dict[List, Any]]:

        rep_manager = tracker.get_slot("rep_manager")

        logger.info( " \n\n\n ")
        logger.info( " =========  action_submit_confirmation  =================  ")
        logger.info( rep_manager )
        logger.info( " \n\n\n ")

        if (rep_manager =="J'accepte"):
            logger.info( " condition 'j\'accepte' verifié : ICI on complete la process")
            task_id = tracker.get_slot("cuisine")
            headers={"content-type": "application/json"}
            uri="http://mvp-test-camunda:8081/engine-rest/task/"+task_id+"/complete"
            # data = json.dumps({"variables": "yo"})
            try:
                first_requete = requests.post(
                        url=uri   ,
                        headers=headers,
                        # data=data,
                        timeout=0.0000000001
                    )
            except requests.exceptions.ReadTimeout: 
                    pass

        dispatcher.utter_message(text="Votre reponse sera transmise au collaborateur ^^ ! ")
        return



class action_ask_confirmation(Action):
    def name(self):
        return "action_ask_confirmation"

    def run(
        self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> List[Dict[List, Any]]:
        collab =  "ilyes" #tracker.get_slot("tracher_id").split("@")[0]
        nbr_jours = tracker.get_slot("mng_nbr_jours")
        date_debut = tracker.get_slot("mng_date_debut")

        rep = "Votre collborateur {}, demande un congé de {} jours à partir du {}".format(collab,nbr_jours,date_debut)

        logger.info( " \n\n\n ")
        logger.info( " =========  action_ask_confirmation  =================  ")
        logger.info( rep )
        logger.info( " \n\n\n ")


        dispatcher.utter_message(text=rep)
        return






