#!/bin/bash -i

VERSION=$1
# RASA_VERSION=$(grep RASA_VERSION ../.env | cut -d "=" -f2)

# if [ -z "$RASA_VERSION" ]; then
#         echo "La valeur de tag de RASA_VERSION dans le fichier .env un cran au-dessus semble vide !"
#         exit 1
# fi
export UID=$(id -u)
export GID=$(id -g)
if [ -z "$VERSION" ]; then
        echo "docker build . -t 7lieues/mvp-test-rasa:${RASA_VERSION}"
        docker build . --build-arg USER=$USER --build-arg UID=$UID --build-arg GID=$GID --build-arg PW=docker -t 7lieues/mvp-test-rasa:${RASA_VERSION}

else
        echo "**************************************************************************"
        echo "On va modifier le paramètre de Version du Build RASA !"
        sleep 1
        echo "docker build . -t 7lieues/mvp-test-rasa:$VERSION"
        docker build --build-arg RASA_VERSION=$VERSION --build-arg USER=$USER --build-arg UID=$UID --build-arg GID=$GID --build-arg PW=docker . -t 7lieues/mvp-test-rasa:$VERSION
fi
