#!/bin/bash -i

VERSION=$1


export UID=$(id -u)
export GID=$(id -g)
if [ -z "$VERSION" ]; then
        echo "docker build . -t 7lieues/mvp-test-rasa-sdk:${RASA_SDK_VERSION}"
        docker build . --build-arg USER=$USER --build-arg UID=$UID --build-arg GID=$GID --build-arg PW=docker -t 7lieues/mvp-test-rasa-sdk:${RASA_SDK_VERSION}
else
        echo "**************************************************************************"
        echo "On va modifier le paramètre de Version du Build RASA-SDK !"
        sleep 1
        echo "docker build . -t 7lieues/mvp-test-rasa-sdk:$VERSION"
        docker build --build-arg RASA_SDK_VERSION=$VERSION --build-arg USER=$USER --build-arg UID=$UID --build-arg GID=$GID --build-arg PW=docker . -t 7lieues/mvp-test-rasa-sdk:$VERSION
fi
